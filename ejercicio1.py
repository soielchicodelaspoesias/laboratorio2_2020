import json


# Obtener los datos del archivo
def obtener_datos(datos):
    j = 0
    archivo = open("top50.csv")
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if j != 0:
            datos.append(linea)
        j += 1
    archivo = open("top50.csv")


def artistas(datos):
    artistas = []
    b = ""
    a = 0
    for i in datos:
        artistas.append(i[2])
    print("Artistas dentro de las top 50 de spotify son: ")
    lista_artistas = (set(artistas))
# listo los artitas y los imprimo
    for i in lista_artistas:
        print(i)
    print("__________________________________________________________________________________________")
# veo cuantas veces se repite cada artista y en el mayor guardo el artista en una variable
    for i in artistas:
        temp = a
        a = artistas.count(i)
        if a > temp:
            b = i
    print("El artista que mas se repite es: ", b, ":", a, "veces")
    print("__________________________________________________________________________________________")


def mediana_ruido(datos):
    mediana = []
    mediana2 = []
    m = 0
    for i in datos:
        mediana.append(int(i[7]))
# ordeno de menos a mayor
    mediana2[:] = sorted(mediana)
    valor_intermedio = mediana2[int((len(mediana2)/2) - 1)] + mediana2[int((len(mediana2)/2))]
# sumo los valores centrales
    m = int(valor_intermedio/2)
    print("La mediana de esta columna es: ", m)
# si la cancion cae dentro de la mediana imprime el nombre de la cancion
    for i in datos:
        if int(i[7]) == m:
            print("la cancion: ", i[1], "cae dentro de la mediana")
    print("__________________________________________________________________________________________")


def bailables_lentas(datos):
    bailables = []
    lentas = []
    for i in datos:
        bailables.append(i[6])
    bailables.sort(reverse=True)
    b1 = bailables[0]
    b2 = bailables[1]
    b3 = bailables[2]
    j = 1
    # ordeno de mayor a menor y tomo los tres valores primeros
    for i in datos:
        # recorro los datos y si es igual a los valores me imprime la cancion
        if i[6] == b1 or i[6] == b2 or i[6] == b3:
            print("genero numero", j, "mas bailable esta: ", i[3])
            j += 1
    print("__________________________________________________________________________________________")
    for i in datos:
        lentas.append(int(i[4]))
# ordeno de menor a mayor y hago lo mismo que arriba
    lentas = sorted(lentas)
    l1 = lentas[0]
    l2 = lentas[1]
    l3 = lentas[2]
    j = 1
    for i in datos:
        if int(i[4]) == l1 or int(i[4]) == l2 or int(i[4]) == l3:
            print("cancion mas lenta numero: ", j, "cancion: ", i[1])
            j += 1
    print("__________________________________________________________________________________________")


def popularidad(datos):
    popularidad = []
    popular = 0
    a = ""
    for i in datos:
        popularidad.append(int(i[13]))
    popularidad[::-1] = sorted(popularidad)
    popular = popularidad[0]
    print(popular)
    for i in datos:
        # saco la cancion mas popular 95 y su ruido es -11 esta dentreo de las menos ruidosas
        if int(i[13]) == popular:
            a = i[1]
            print("La cancion mas popular es: ", a, "y su tempo es de: ", i[7])
            print("esta entre las menos ruidosas")
    for i in datos:
        # enlisto lo que piden
        if int(i[13]) == popular:
            print("Artista: ", i[2], "Cancion: ", i[1], "Popularidad: ", i[13], "Ruido:", i[6])
    print("__________________________________________________________________________________________")


def json(datos, top50):
    ranking = ["Track.Name", "Artist.Name", "Genre", "Beats.Per.Minute", "Energy", "Danceability",
               "Loudness..dB..", "Liveness", "Valence.", "Length.", "Acousticness..", "Speechiness."
        , "Popularity"]
    # creo listas con lo pedido y las agrego a un diccionario el cual paso a json despues
    canciones = []
    artistas = []
    generos = []
    beat = []
    length = []
    popularity = []
    for i in datos:
        canciones.append(i[1])
        artistas.append(i[2])
        generos.append(i[3])
        beat.append(i[4])
        length.append(i[10])
        popularity.append((i[13]))
    top50["Ranking"] = ranking
    top50["cancion"] = canciones
    top50["artistas"] = artistas
    top50["generos"] = generos
    top50["beats"] = beat
    top50["length"] = length
    top50["popularity"] = popularity
    save_json(top50)


def save_json(top50):
    with open('top50.json', 'w') as fichero:
        json.dump(top50, fichero, indent=4)


datos = []
top50 = {}
obtener_datos(datos)
artistas(datos)
mediana_ruido(datos)
bailables_lentas(datos)
popularidad(datos)
json(datos, top50)
